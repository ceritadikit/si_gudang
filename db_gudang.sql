-- phpMyAdmin SQL Dump
-- version 4.6.0
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Sep 03, 2016 at 06:33 AM
-- Server version: 10.1.9-MariaDB-log
-- PHP Version: 5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_gudang`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id_barang` int(11) NOT NULL,
  `kode_barang` varchar(30) NOT NULL,
  `nama_barang` varchar(50) NOT NULL,
  `qty` int(11) NOT NULL,
  `rak` varchar(30) NOT NULL,
  `id_suplier` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id_barang`, `kode_barang`, `nama_barang`, `qty`, `rak`, `id_suplier`) VALUES
(1, 'CXT-766', 'Gear Aloy', 90, 'C3', 1),
(4, 'WRR-267', 'TuroBoro', 131, 'R1', 1),
(5, 'HJO-990', 'Haseen Jumbo ', 229, 'C1-A', 1),
(6, 'CVX-9991', 'Block CVT', 31, 'C-7', 1),
(8, 'CXY-011', 'Coaxial', 88, 'C9', 3);

-- --------------------------------------------------------

--
-- Table structure for table `produksi`
--

CREATE TABLE `produksi` (
  `id_produksi` int(11) NOT NULL,
  `date_produksi` date NOT NULL,
  `nama_produksi` varchar(150) NOT NULL,
  `memo` text NOT NULL,
  `status` enum('hold','produksi') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produksi`
--

INSERT INTO `produksi` (`id_produksi`, `date_produksi`, `nama_produksi`, `memo`, `status`) VALUES
(1, '2016-09-03', 'PT.Intan berlian - Batch 1', 'Produksi untuk PT.Intan berlian tahap ke-1 dari 9 tahap yang tertanda tangan di kontrak', 'hold'),
(2, '2016-09-04', 'PT.Intan berlian - Batch 2', 'Produksi untuk PT.Intan berlian tahap ke-2 dari 9 tahap yang tertanda tangan di kontrak.\r\n\r\nProduksi tahap ke-1 sudah selesia, terdapat beberapa kekurangan stock barang', 'hold'),
(3, '2016-09-05', 'Produksi untuk PT DI', 'pembuatan 10 suku cadang', 'produksi');

-- --------------------------------------------------------

--
-- Table structure for table `request`
--

CREATE TABLE `request` (
  `id_request` int(11) NOT NULL,
  `kode_request` varchar(20) NOT NULL,
  `date_request` date NOT NULL,
  `deadline` date NOT NULL,
  `memo` text NOT NULL,
  `status` enum('pending','terkirim','ditolak','disetujui') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `request`
--

INSERT INTO `request` (`id_request`, `kode_request`, `date_request`, `deadline`, `memo`, `status`) VALUES
(1, 'JU-AX16', '2016-08-31', '0000-00-00', 'Produksi untuk PT.Intan berlian tahap ke-1 dari 9 tahap yang tertanda tangan di kontrak', 'disetujui'),
(2, 'A002', '2016-08-31', '2016-09-02', 'Butuh cepat', 'disetujui'),
(3, 'A003', '2016-08-31', '2016-09-03', 'Lagi ada diskon, pesen yang ini kebetulan stock juga sudah menipis', 'ditolak'),
(4, 'Z019', '2016-08-31', '2016-09-01', 'Stock habis', 'disetujui'),
(5, 'A90', '2016-08-31', '2016-09-09', 'Test aja', 'disetujui'),
(6, 'CTR', '2016-09-02', '2016-09-04', 'proses', 'pending');

-- --------------------------------------------------------

--
-- Table structure for table `sub_produksi`
--

CREATE TABLE `sub_produksi` (
  `id_sub_produksi` int(11) NOT NULL,
  `id_produksi` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_produksi`
--

INSERT INTO `sub_produksi` (`id_sub_produksi`, `id_produksi`, `id_barang`, `qty`) VALUES
(1, 1, 5, 19),
(2, 1, 1, 10),
(3, 1, 8, 12),
(4, 2, 5, 12),
(5, 2, 4, 20),
(11, 3, 1, 10),
(13, 3, 5, 12);

-- --------------------------------------------------------

--
-- Table structure for table `sub_request`
--

CREATE TABLE `sub_request` (
  `id_sub_request` int(11) NOT NULL,
  `id_request` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_request`
--

INSERT INTO `sub_request` (`id_sub_request`, `id_request`, `id_barang`, `qty`) VALUES
(1, 1, 4, 10),
(7, 1, 1, 22),
(14, 1, 1, 22),
(15, 2, 5, 50),
(16, 5, 6, 24),
(17, 5, 8, 78),
(18, 1, 4, 234),
(19, 1, 6, 11),
(20, 1, 4, 45),
(21, 1, 4, 21),
(22, 1, 5, 55),
(23, 6, 5, 12);

-- --------------------------------------------------------

--
-- Table structure for table `suplier`
--

CREATE TABLE `suplier` (
  `id_suplier` int(11) NOT NULL,
  `nama_suplier` varchar(30) NOT NULL,
  `alamat_suplier` varchar(100) NOT NULL,
  `email_suplier` varchar(30) NOT NULL,
  `tlpn_suplier` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suplier`
--

INSERT INTO `suplier` (`id_suplier`, `nama_suplier`, `alamat_suplier`, `email_suplier`, `tlpn_suplier`) VALUES
(1, 'CV. Sarana Buana Perkasa', 'Teluk Cijamber', 'kontak_suplier@sbp.co.id', '0889133411'),
(3, 'PT. Graha Manunggal Djayati', 'Kebon Kopi', 'suplier@manunggal.co.id', '022980019'),
(4, 'CV. Anugrah Jaya', 'Cileunyi', 'Suplier_aj@gmail.com', '08871817782');

-- --------------------------------------------------------

--
-- Table structure for table `user_auth`
--

CREATE TABLE `user_auth` (
  `id_auth` int(11) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `level` enum('gudang','keuangan','produksi') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_auth`
--

INSERT INTO `user_auth` (`id_auth`, `email`, `password`, `level`) VALUES
(1, 'gudang@mail.com', 'gudang', 'gudang'),
(2, 'keuangan@mail.com', 'keuangan', 'keuangan'),
(3, 'produksi@mail.com', 'produksi', 'produksi');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_barang`),
  ADD KEY `id_suplier` (`id_suplier`);

--
-- Indexes for table `produksi`
--
ALTER TABLE `produksi`
  ADD PRIMARY KEY (`id_produksi`);

--
-- Indexes for table `request`
--
ALTER TABLE `request`
  ADD PRIMARY KEY (`id_request`);

--
-- Indexes for table `sub_produksi`
--
ALTER TABLE `sub_produksi`
  ADD PRIMARY KEY (`id_sub_produksi`),
  ADD KEY `id_produksi` (`id_produksi`),
  ADD KEY `id_barang` (`id_barang`);

--
-- Indexes for table `sub_request`
--
ALTER TABLE `sub_request`
  ADD PRIMARY KEY (`id_sub_request`),
  ADD KEY `id_request` (`id_request`),
  ADD KEY `id_barang` (`id_barang`);

--
-- Indexes for table `suplier`
--
ALTER TABLE `suplier`
  ADD PRIMARY KEY (`id_suplier`);

--
-- Indexes for table `user_auth`
--
ALTER TABLE `user_auth`
  ADD PRIMARY KEY (`id_auth`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id_barang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `produksi`
--
ALTER TABLE `produksi`
  MODIFY `id_produksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `request`
--
ALTER TABLE `request`
  MODIFY `id_request` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `sub_produksi`
--
ALTER TABLE `sub_produksi`
  MODIFY `id_sub_produksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `sub_request`
--
ALTER TABLE `sub_request`
  MODIFY `id_sub_request` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `suplier`
--
ALTER TABLE `suplier`
  MODIFY `id_suplier` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `user_auth`
--
ALTER TABLE `user_auth`
  MODIFY `id_auth` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `barang`
--
ALTER TABLE `barang`
  ADD CONSTRAINT `barang_ibfk_1` FOREIGN KEY (`id_suplier`) REFERENCES `suplier` (`id_suplier`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sub_produksi`
--
ALTER TABLE `sub_produksi`
  ADD CONSTRAINT `sub_produksi_ibfk_1` FOREIGN KEY (`id_produksi`) REFERENCES `produksi` (`id_produksi`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sub_produksi_ibfk_2` FOREIGN KEY (`id_barang`) REFERENCES `barang` (`id_barang`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sub_request`
--
ALTER TABLE `sub_request`
  ADD CONSTRAINT `sub_request_ibfk_2` FOREIGN KEY (`id_barang`) REFERENCES `barang` (`id_barang`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sub_request_ibfk_3` FOREIGN KEY (`id_request`) REFERENCES `request` (`id_request`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
