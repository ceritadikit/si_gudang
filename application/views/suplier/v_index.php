<link href="<?= base_url('assets/js/datatables/jquery.dataTables.min.css') ?>" rel="stylesheet" type="text/css" />
<link href="<?= base_url('assets/js/datatables/responsive.bootstrap.min.css') ?>" rel="stylesheet" type="text/css" />

<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Data Suplier</h2>
				<div class="pull-right">
					<div class="input-group">
						<a href="<?= base_url() ?>suplier/add" class="btn btn-primary">Tambah Suplier</a>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<?php if ($this->session->flashdata('pesan') != null): ?>
				<?php echo $this->session->flashdata('pesan'); ?>
			<?php endif ?>
			<div class="x_content">
				<table id="datatable" class="table table-striped table-bordered">
					<thead>
						<tr>
							<th class="col-md-1">No.</th>
							<th class="col-md-3">Nama</th>
							<th class="col-md-4">Alamat</th>
							<th class="col-md-2">Email</th>
							<th class="col-md-2">Phone</th>
							<th class="col-md-1">Aksi</th>
						</tr>
					</thead>

					<tbody>
						<?php $nomor = 1; ?>
						<?php foreach ($dataSuplier as $key): ?>
							<tr>
								<td><?= $nomor++ ?></td>
								<td><?= $key->nama_suplier ?></td>
								<td><?= $key->alamat_suplier ?></td>
								<td><?= $key->email_suplier ?></td>
								<td><?= $key->tlpn_suplier ?></td>
								<td align="center">
									<a href="<?= base_url('suplier/' . $key->id_suplier) ?>" title="Edit" class="btn btn-info fa fa-pencil-square-o"></a>
									<a href="<?= base_url('suplier/delete/' . $key->id_suplier) ?>" onclick="return confirm('Are you sure?')" title="Delete" class="btn btn-danger fa fa-trash"></a>
								</td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- Datatables-->
<script src="<?= base_url(); ?>assets/js/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url(); ?>assets/js/datatables/dataTables.bootstrap.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#datatable').dataTable();
	});
</script>
