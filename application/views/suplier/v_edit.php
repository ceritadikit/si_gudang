<div class="row">
	<div class="col-md-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Edit Suplier : <?= $dataSuplier->nama_suplier ?></h2>
				<div class="clearfix"></div>
			</div>

			<div class="x_content">
				<form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="<?= base_url('SuplierController/updateSuplier') ?>" method="POST">
					<input type="hidden" name="id_suplier" value="<?= $dataSuplier->id_suplier ?>">

					<div class="form-group col-md-12">
						<label class="control-label col-md-1">Nama <span class="required">*</span>
						</label>
						<div class="col-md-5">
							<input type="text" name="nama_suplier" required="required" value="<?= $dataSuplier->nama_suplier ?>" class="form-control">
						</div>
					</div>

					<div class="form-group col-md-12">
						<label class="control-label col-md-1">alamat <span class="required">*</span>
						</label>
						<div class="col-md-5">
							<input type="text" name="alamat_suplier"  value="<?= $dataSuplier->alamat_suplier ?>" required="required" class="form-control col-md-7 col-xs-12">
						</div>
					</div>

					<div class="form-group col-md-12">
						<label class="control-label col-md-1">Email <span class="required">*</span>
						</label>
						<div class="col-md-5">
							<input type="text" name="email_suplier"  value="<?= $dataSuplier->email_suplier ?>" required="required" class="form-control col-md-7 col-xs-12">
						</div>
					</div>

					<div class="form-group col-md-12">
						<label class="control-label col-md-1">Telepon <span class="required">*</span>
						</label>
						<div class="col-md-5">
							<input type="text" name="tlpn_suplier"  value="<?= $dataSuplier->tlpn_suplier ?>" required="required" class="form-control col-md-7 col-xs-12">
						</div>
					</div>

					<div class="ln_solid col-md-12"></div>

					<div class="form-group col-md-6">
						<div class="pull-right">
							<button type="button" class="btn btn-default" onClick="history.go(-1);return true;">Cancel</button>
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>
</div>
