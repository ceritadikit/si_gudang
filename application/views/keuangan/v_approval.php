<div class="row">
	<div class="col-md-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Daftar Item Request - <?= $dataRequest->kode_request ?></h2>
				<div class="clearfix"></div>
			</div>
        <div class="x_content">
          <form id="demo-form2" data-parsley-validate class="form-horizontal" action="<?= base_url('KeuanganController/approvalRequest') ?>" method="POST">
          <table id="datatable" class="table table-striped table-bordered">
  					<thead>
  						<tr>
  							<th>No.</th>
  							<th class="col-md-5">Kode Barang</th>
  							<th class="col-md-3">Nama Barang</th>
								<th class="col-md-3">Qty Asal</th>
  							<th class="col-md-3">Qty Order</th>
  						</tr>
  					</thead>
  					<tbody>
  						<?php $nomor = 1; ?>
  						<?php if(is_array($dataSubRequest)): ?>
  						<?php foreach ($dataSubRequest as $key): ?>
  							<tr>
  								<td> <input readonly style="border:none" type="text" name="nomor" value="<?= $nomor ?>"> </td>
  								<td> <input readonly style="border:none" type="text" name="kode_barang" value="<?= $key->kode_barang ?>"> </td>
    								   <input readonly style="border:none" type="hidden" name="id_barang[<?= $nomor ?>]" id="id_barang" value="<?= $key->id_barang ?>">
  								<td> <input readonly style="border:none" type="text" value="<?= $key->nama_barang ?>"> </td>
  								<td> <input readonly style="border:none" type="text" name="qty2[<?= $nomor ?>]" id="qty" value="<?= $key->qty2 ?>"> </td>
  								<td> <input readonly style="border:none" type="text" name="qty[<?= $nomor ?>]" id="qty" value="<?= $key->qty ?>"> </td>
  							</tr>
								<?php $nomor++ ?>
  						<?php endforeach ?>
  						<?php endif; ?>
  					</tbody>
					</table>
        	<div class="ln_solid col-md-12"></div>
	        <div class="form-group col-md-12">
	          <div class="pull-right">
	            <input type="hidden" name="id_request" value="<?= $dataRequest->id_request?>">
	            <a href="<?= base_url('KeuanganController/permintaan') ?>"  class="btn btn-default">Cancel</a>
	            <button type="submit" class="btn btn-primary">Submit</button>
	          </div>
	        </div>
				</form>
      </div>
    </div>
  </div>
</div>

<script>
function goBack() {
    window.history.back();
}
</script>
