<link href="<?= base_url('assets/js/datatables/jquery.dataTables.min.css') ?>" rel="stylesheet" type="text/css" />
<link href="<?= base_url('assets/js/datatables/responsive.bootstrap.min.css') ?>" rel="stylesheet" type="text/css" />

<div class="row">
	<div class="col-md-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Data Request Barang</h2>
				<div class="clearfix"></div>
			</div>
			<?php if ($this->session->flashdata('pesan') != null): ?>
                <?php echo $this->session->flashdata('pesan'); ?>
            <?php endif ?>
			<div class="x_content">
				<table id="datatable" class="table table-striped table-bordered">
					<thead>
						<tr>
							<th class="col-md-1">No.</th>
							<th class="col-md-1">kode Request</th>
							<th class="col-md-1">Date Request</th>
							<th class="col-md-1">Deadline</th>
							<th class="col-md-5">Memo</th>
							<th class="col-md-1">Status</th>
							<th class="col-md-2">Aksi</th>
						</tr>
					</thead>

					<tbody>
						<?php $nomor = 1; ?>
						<?php foreach ($dataPermintaan as $key): ?>
							<tr>
								<td><?= $nomor++ ?></td>
								<td><?= $key->kode_request ?></td>
								<td><?= $key->date_request ?></td>
								<td><?= $key->deadline ?></td>
								<td><?= $key->memo ?></td>
								<td><?= $key->status ?></td>
								<td align="center">
									<ul>
										<a href="<?= base_url('keuanganController/ditailItemRequest/' . $key->id_request) ?>" title="Terima Request" class="btn btn-primary fa fa-check"></a>
										<a href="<?= base_url('keuanganController/tolakPesanan/' . $key->id_request) ?>" onclick="return confirm('Are you sure?')" title="Tolak Request" class="btn btn-danger fa fa-times"></a>
									</ul>
								</td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- Start modal ditail sub item -->
<?php foreach ($dataPermintaan as $key): ?>
	<form id="demo-form2" data-parsley-validate class="form-horizontal" action="<?= base_url('KeuanganController/updateItemRequest/'.$key->id_request)?>"  method="POST">
<div class="modal fade" id="modal_ditail_<?php echo $key->id_request; ?>">"
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" area-hidden="true">&times;</button>
                <h4 class="modal-title">Ditail Sub Item - <?php echo $key->kode_request; ?></h4>
            </div>
                <div class="modal-body" style="margin-bottom:-20px;">
									<div class="row">
									<?php $no = 1; ?>
									<?php foreach ($dataItemRequest as $key2): ?>
										<?php
										$id_item = $key->id_request;
										$id_sub_item = $key2->id_request;
										if ($id_item == $id_sub_item){
										?>
										<div class="col-md-1 col-md-offset-1">
												<h3><?php echo $no; ?></h3>
												<input type="hidden" maxlength="20" name="nomor" id="nomor" value="<?php echo $no; ?>" style="border: none; border-color: transparent;">
										</div>
										<div class="col-md-9">
											<ul>
												<li class="list-group-item">
													<div class="col-md-5">Kode Barang : </div>
													<input type="text" maxlength="20" value="<?= $key2->kode_barang ?>" style="border: none; border-color: transparent;">
													<input type="hidden" maxlength="20" name="id_barang[]" value="<?= $key2->id_barang ?>" style="border: none; border-color: transparent;">
												<li class="list-group-item">
													<div class="col-md-5">Nama Barang : </div>
													<input type="text" maxlength="20" value="<?= $key2->nama_barang ?>" style="border: none; border-color: transparent;">
												</li>
												<li class="list-group-item">
													<div class="col-md-5">Qty Pesanan : </div>
													<input typeclass="list-group-item"="text" maxlength="20" name="qty[]" value="<?= $key2->qty ?>" style="border: none; border-color: transparent;">
												</li>
											</ul>
										</div>

										<?php $no++; } ?>
									<?php	endforeach ?>
									<?php $count = $no-1; ?>
									<input type="hidden" class="list-group-item"="text" maxlength="20" name="nomor" value="<?= $count; ?>" style="border: none; border-color: transparent;">

							</div>
                </div> <!-- end modal-body -->
                <div class="modal-footer" style="margin-bottom:-10px;">
                    <button type="button" class="btn btn-default btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-default btn-success" >Setuju</button>
                </div>
        </div>
    </div>
</div>
<?php endforeach ?>
</form>
<!-- End modal ditail sub item -->

<!-- Datatables-->
<script src="<?= base_url('assets/js/datatables/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/js/datatables/dataTables.bootstrap.js') ?>"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#datatable').dataTable();
	});
</script>
