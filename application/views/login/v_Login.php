<!DOCTYPE html>
<html>
<head>
<title>Login Sistem Informasi Gudang</title>
<link href="<?= base_url('assets\simple_login_form\css\style.css')?>" rel='stylesheet' type='text/css' />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
</script>
<!--webfonts-->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,300,600,700' rel='stylesheet' type='text/css'>
<!--//webfonts-->
</head>
<body>

<!--/start-login-one-->
<h1>Login SI Gudang</h1>
<<h3><?php if ($this->session->flashdata('pesan') != null): ?>
		<?php echo $this->session->flashdata('pesan'); ?>
<?php endif ?></h3>

		<div class="login">
			<div class="ribbon-wrapper h2 ribbon-red">
				<div class="ribbon-front">
					<h2>User Login</h2>
				</div>

				<div class="ribbon-edge-topleft2"></div>
				<div class="ribbon-edge-bottomleft"></div>
			</div>
			<form action="LoginController/login" method="post" >
				<ul>
					<li>
						<input type="email" name="email" placeholder="Email Address" required=""><a href="#" class=" icon user"></a>
					</li>
					 <li>
						<input type="password" name="password" placeholder="Password" required=""><a href="#" class=" icon lock"></a>
					</li>
				</ul>
				<div class="submit">
					<input type="submit" class="btn btn-primary btn-block"  value="login"></input>
				</div>
			</form>
		</div>
</body>
</html>
