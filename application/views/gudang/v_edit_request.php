<div class="row">
	<div class="col-md-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Tambah Request Barang - <?= $dataRequest->kode_request ?></h2>
				<div class="clearfix"></div>
			</div>

			<div class="x_content">
				<form id="demo-form2" data-parsley-validate class="form-horizontal" action="<?= base_url('RequestController/updateRequest') ?>" method="POST">
          <div class="form-group col-md-12">
						<label class="control-label col-md-2">Kode Request <span class="required">*</span></label>
						<div class="col-md-2">
							<?php if ($dataRequest->status == 'terkirim'): ?>
								<input type="text" disabled value="<?= $dataRequest->kode_request?>" name="kode_request" class="form-control">
							<?php else: ?>
								<input type="text" maxlength="20" value="<?= $dataRequest->kode_request?>" name="kode_request" required="required" class="form-control">
							<?php endif; ?>

						</div>
            <label class="control-label col-md-2">Deadline <span class="required">*</span></label>
						<div class="col-md-2">
              <?php
							$hari_ini=date("Y-m-d");
							?>
							<?php if ($dataRequest->status == 'terkirim'): ?>
								<input type="date" disabled name="deadline" value="<?= $dataRequest->deadline?>" required="required" min=<?php echo"$hari_ini"; ?> class="form-control">
							<?php else: ?>
								<input type="date" name="deadline" value="<?= $dataRequest->deadline?>" required="required" min=<?php echo"$hari_ini"; ?> class="form-control">
							<?php endif; ?>

						</div>
					</div>

					<div class="form-group col-md-12">
						<label class="control-label col-md-2">Memo</label>
						<div class="col-md-6">
              <textarea class="form-control" rows="6" name="memo" placeholder="berikan catatan atau keterangan untuk bagian yang terkait"><?= $dataRequest->memo?></textarea>
						</div>
					</div>

					<div class="ln_solid col-md-12"></div>
					<div class="form-group col-md-8">
						<div class="pull-right">
              <input type="hidden" name="date_request" value="<?= $hari_ini ?>">
              <input type="hidden" name="id_request" value="<?= $dataRequest->id_request?>">
              <input type="hidden" name="status" value="pending">
							<button type="button" class="btn btn-default" onClick="history.go(-1);return true;">Cancel</button>
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
