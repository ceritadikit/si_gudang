<link href="<?= base_url('assets/js/datatables/jquery.dataTables.min.css') ?>" rel="stylesheet" type="text/css" />
<link href="<?= base_url('assets/js/datatables/responsive.bootstrap.min.css') ?>" rel="stylesheet" type="text/css" />

<div class="row">
	<div class="col-md-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Data Barang</h2>
				<div class="pull-right">
					<div class="input-group">
						<a href="<?= base_url('BarangController/addBarang') ?>" class="btn btn-primary">Tambah Barang</a>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<?php if ($this->session->flashdata('pesan') != null): ?>
                <?php echo $this->session->flashdata('pesan'); ?>
            <?php endif ?>
			<div class="x_content">
				<table id="datatable" class="table table-striped table-bordered">
					<thead>
						<tr>
							<th class="col-md-1">No.</th>
							<th class="col-md-3">Kode</th>
							<th class="col-md-5">Nama</th>
							<th class="col-md-1">Qty</th>
							<th class="col-md-1">Rak</th>
							<th class="col-md-1">Aksi</th>
						</tr>
					</thead>

					<tbody>
						<?php $nomor = 1; ?>
						<?php foreach ($dataBarang as $key): ?>
							<tr>
								<td><?= $nomor++ ?></td>
								<td><?= $key->kode_barang ?></td>
								<td><?= $key->nama_barang ?></td>
								<td><?= $key->qty ?></td>
								<td><?= $key->rak ?></td>
								<td align="center">
									<a href="<?= base_url('BarangController/editBarang/' . $key->id_barang) ?>" title="Edit" class="btn btn-info fa fa-pencil-square-o"></a>
									<a href="<?= base_url('BarangController/deleteBarang/' . $key->id_barang) ?>" onclick="return confirm('Are you sure?')" title="Delete" class="btn btn-danger fa fa-trash"></a>
								</td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- Datatables-->
<script src="<?= base_url('assets/js/datatables/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/js/datatables/dataTables.bootstrap.js') ?>"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#datatable').dataTable();
	});
</script>
