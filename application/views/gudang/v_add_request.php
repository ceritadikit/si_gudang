<div class="row">
	<div class="col-md-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Tambah Request Barang</h2>
				<div class="clearfix"></div>
			</div>

			<div class="x_content">
				<form id="demo-form2" data-parsley-validate class="form-horizontal" action="<?= base_url('RequestController/insertRequest') ?>" method="POST">
          <div class="form-group col-md-12">
						<label class="control-label col-md-2">Kode Request <span class="required">*</span></label>
						<div class="col-md-2">
							<input type="text" maxlength="20" name="kode_request" required="required" class="form-control">
						</div>
            <label class="control-label col-md-2">Deadline <span class="required">*</span></label>
						<div class="col-md-2">
              <?php
							$hari_ini=date("Y-m-d");
							?>
							<input type="date" name="deadline" required="required" min=<?php echo"$hari_ini"; ?> class="form-control">
						</div>
					</div>

					<div class="form-group col-md-12">
						<label class="control-label col-md-2">Memo</label>
						<div class="col-md-6">
              <textarea class="form-control" rows="6" name="memo" placeholder="berikan catatan atau keterangan untuk bagian yang terkait"></textarea>
						</div>
					</div>

					<div class="ln_solid col-md-12"></div>
					<div class="form-group col-md-8">
						<div class="pull-right">
              <input type="hidden" name="date_request" value="<?= $hari_ini ?>">
              <input type="hidden" name="status" value="pending">
							<button type="button" class="btn btn-default" onClick="history.go(-1);return true;">Cancel</button>
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
