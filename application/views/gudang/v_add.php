<div class="row">
	<div class="col-md-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Tambah Barang</h2>
				<div class="clearfix"></div>
			</div>

			<div class="x_content">
				<form id="demo-form2" data-parsley-validate class="form-horizontal" action="<?= base_url('BarangController/insertBarang') ?>" method="POST">
					<div class="form-group col-md-12">
						<label class="control-label col-md-2">Kode Barang <span class="required">*</span></label>
						<div class="col-md-2">
							<input type="text" maxlength="30" name="kode_barang" required="required" max="4" class="form-control">
						</div>
					</div>

					<div class="form-group col-md-12">
						<label class="control-label col-md-2">Nama Barang <span class="required">*</span></label>
						<div class="col-md-6">
							<input type="text" maxlength="50" name="nama_barang" required="required" class="form-control">
						</div>
					</div>

          <div class="form-group col-md-12">
						<label class="control-label col-md-2">Qty Barang <span class="required">*</span></label>
						<div class="col-md-2">
							<input type="text" name="qty" required="required" class="form-control">
						</div>
            <label class="control-label col-md-2">Rak Barang <span class="required">*</span></label>
            <div class="col-md-2">
              <input type="text" name="rak" required="required" class="form-control">
            </div>
					</div>

          <div class="form-group col-md-12">
						<label class="control-label col-md-2">Suplier Barang <span class="required">*</span></label>
						<div class="col-md-6">
							<div class="form-group">
							  <select class="form-control" name="id_suplier" id="sel1">
									<?php foreach ($dataSuplier->result() as $key => $val): ?>
							    <option value="<?= $val->id_suplier;?>"><?= $val->nama_suplier;?></option>
									<?php endforeach; ?>
							  </select>
							</div>
						</div>
					</div>

					<div class="ln_solid col-md-12"></div>

					<div class="form-group col-md-8">
						<div class="pull-right">
							<button type="button" class="btn btn-default" onClick="history.go(-1);return true;">Cancel</button>
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
