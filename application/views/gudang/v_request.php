<link href="<?= base_url('assets/js/datatables/jquery.dataTables.min.css') ?>" rel="stylesheet" type="text/css" />
<link href="<?= base_url('assets/js/datatables/responsive.bootstrap.min.css') ?>" rel="stylesheet" type="text/css" />

<div class="row">
	<div class="col-md-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Data Request Barang</h2>
				<div class="pull-right">
					<div class="input-group">
						<a href="<?= base_url('RequestController/addRequest') ?>" class="btn btn-primary">Tambah Request</a>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<?php if ($this->session->flashdata('pesan') != null): ?>
                <?php echo $this->session->flashdata('pesan'); ?>
            <?php endif ?>
			<div class="x_content">
				<table id="datatable" class="table table-striped table-bordered">
					<thead>
						<tr>
							<th class="col-md-1">No.</th>
							<th class="col-md-1">kode Request</th>
							<th class="col-md-1">Date Request</th>
							<th class="col-md-1">Deadline</th>
							<th class="col-md-5">Memo</th>
							<th class="col-md-1">Status</th>
							<th class="col-md-2">Aksi</th>
						</tr>
					</thead>

					<tbody>
						<?php $nomor = 1; ?>
						<?php foreach ($dataRequest as $key): ?>
							<tr>
								<td><?= $nomor++ ?></td>
								<td><?= $key->kode_request ?></td>
								<td><?= $key->date_request ?></td>
								<td><?= $key->deadline ?></td>
								<td><?= $key->memo ?></td>
								<td><?= $key->status ?></td>
								<td align="center">
									<ul>
										<?php if ($key->status == 'terkirim'): ?>
											<button type="button" disabled title="add item" class="btn btn-default fa fa-plus-circle"></button>
											<button type="button" disabled title="send request" class="btn btn-default fa fa-send"></button>
										<?php else: ?>
											<a href="<?= base_url('RequestController/addSubRequest/' . $key->id_request) ?>" title="add item" class="btn btn-primary fa fa-plus-circle"></a>
											<a href="<?= base_url('RequestController/sendRequest/' . $key->id_request) ?>" onclick="return confirm('Are you sure?')" title="send request" class="btn btn-warning fa fa-send"></a>
										<?php endif; ?>
									</ul>

									<ul>
										<a href="<?= base_url('RequestController/editRequest/' . $key->id_request) ?>" title="Edit" class="btn btn-info fa fa-pencil-square-o"></a>
										<a href="<?= base_url('RequestController/deleteRequest/' . $key->id_request) ?>" onclick="return confirm('Are you sure?')" title="Delete" class="btn btn-danger fa fa-trash"></a>
									</ul>
								</td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- Datatables-->
<script src="<?= base_url('assets/js/datatables/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/js/datatables/dataTables.bootstrap.js') ?>"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#datatable').dataTable();
	});
</script>
