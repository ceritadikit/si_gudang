<div class="col-md-3 left_col">
	<div class="left_col scroll-view">

		<div class="navbar nav_title" style="border: 0;">
			<a href="<?= base_url() ?>" class="site_title"><i class="fa fa-clock-o"></i> <span>SI Gudang</span></a>
		</div>

		<div class="clearfix"></div>

		<!-- sidebar menu -->
		<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
			<div class="menu_section">
				<ul class="nav side-menu">
					<li>
						<a href="<?= base_url('BerandaController') ?>"><i class="fa fa-home"></i> Home </a>
					</li>
					<li>
						<a><i class="fa fa-database"></i> Data Gudang <span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu" style="display: none">
							<li>
								<a href="<?= base_url('BarangController') ?>">
									<i class="fa fa-archive"></i> Data Barang
								</a>
							</li>
							<li>
								<a href="<?= base_url('SuplierController') ?>">
									<i class="fa fa-user"></i> Suplier
								</a>
							</li>
						</ul>
					</li>
					<li>
						<a><i class="fa fa-truck"></i> Request Barang <span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu" style="display: none">
							<li>
								<a href="<?= base_url('RequestController') ?>">
									<i class="fa fa-upload"></i> Request
								</a>
							</li>
							<li>
								<a href="<?= base_url('RequestController/sukses') ?>">
									<i class="fa fa-check-circle"></i> Sukses
								</a>
							</li>
							<li>
								<a href="<?= base_url('RequestController/ditolak') ?>">
									<i class="fa fa-times"></i> Ditolak
								</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div> <!-- /sidebar menu -->
	</div> <!-- /.left_col scroll-view -->
</div> <!-- /.col-md-3 left_col -->
