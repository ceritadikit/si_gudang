<div class="col-md-3 left_col">
	<div class="left_col scroll-view">

		<div class="navbar nav_title" style="border: 0;">
			<a href="<?= base_url() ?>" class="site_title"><i class="fa fa-clock-o"></i> <span>SI Gudang</span></a>
		</div>

		<div class="clearfix"></div>

		<!-- sidebar menu -->
		<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
			<div class="menu_section">
				<ul class="nav side-menu">
					<li>
						<a href="<?= base_url('BerandaController') ?>"><i class="fa fa-home"></i> Home </a>
					</li>
					<li>
						<a><i class="fa fa-list-ul"></i> Data Keuangan <span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu" style="display: none">
							<li>
								<a href="<?= base_url('KeuanganController/permintaan') ?>">
									<i class="fa fa-globe"></i> Permintaan Barang
								</a>
							</li>
							<li>
								<a href="<?= base_url('KeuanganController/pembelian') ?>">
									<i class="fa fa-globe"></i> Pembelian Barang
								</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div> <!-- /sidebar menu -->
	</div> <!-- /.left_col scroll-view -->
</div> <!-- /.col-md-3 left_col -->
