<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- bootstrap -->
<link href="<?= base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">

<!-- font awesome -->
<link href="<?= base_url('assets/fonts/css/font-awesome.min.css') ?>" rel="stylesheet">

<!-- Custom styling plus plugins -->
<link href="<?= base_url('assets/css/custom.css') ?>" rel="stylesheet" />

<!-- javascript -->
<script type="text/javascript" src="<?= base_url('assets/js/jquery.min.js') ?>"></script>