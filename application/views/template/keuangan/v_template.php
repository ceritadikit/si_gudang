<!DOCTYPE html>
<html>
<head>
	<title>SI Gudang</title>
	<?php $this->load->view('template/keuangan/v_header'); ?>
</head>
<body class="nav-md">
	<div class="container body">
		<div class="main_container">

			<?php $this->load->view('template/keuangan/v_left_menu'); ?>
			<?php $this->load->view('template/keuangan/v_top_menu'); ?>

			<!-- page content -->
			<div class="right_col" role="main">
				<?php $this->load->view($content); ?>
			</div>

		</div> <!-- /.main_container -->
	</div> <!-- /.container body -->

	<div id="custom_notifications" class="custom-notifications dsp_none">
		<ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
		</ul>
		<div class="clearfix"></div>
		<div id="notif-group" class="tabbed_notifications"></div>
	</div>

	<?php $this->load->view('template/keuangan/v_footer'); ?>
</body>
</html>
