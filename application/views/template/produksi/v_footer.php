<!-- bootstrap -->
<script type="text/javascript" src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>

<!-- bootstrap progress js -->
<script type="text/javascript" src="<?= base_url('assets/js/progressbar/bootstrap-progressbar.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/js/nicescroll/jquery.nicescroll.min.js') ?>"></script>

<!-- custom javascript -->
<script type="text/javascript" src="<?= base_url('assets/js/custom.js') ?>"></script>

<!-- pace -->
<!-- animasi loading top navigation -->
<script type="text/javascript" src="<?= base_url('assets/js/pace/pace.min.js') ?>"></script>