<div class="row">
	<div class="col-md-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Tambah Item - <?= $dataProduksi->nama_produksi ?></h2>
				<div class="clearfix"></div>
			</div>

			<?php if ($this->session->flashdata('pesan') != null): ?>
          <?php echo $this->session->flashdata('pesan'); ?>
      <?php endif ?>

			<div class="x_content">
				<form id="demo-form2" data-parsley-validate class="form-horizontal" action="<?= base_url('ProduksiController/insertSubProduksi') ?>" method="POST">
					<div class="form-group col-md-12">
						<label class="control-label col-md-2">Item <span class="required">*</span></label>
						<div class="col-md-6">
							<div class="form-group">
							  <select class="form-control" name="id_barang" id="sel1">
									<?php foreach ($dataBarang as $key => $value): ?>
										<option value="<?= $value->id_barang?> | <?= $value->qty?>"><?= $value->nama_barang?></option><hr>
									<?php endforeach; ?>
							  </select>
							</div>
						</div>

					</div>
					<div class="form-group col-md-12">
						<label class="control-label col-md-2">qty order <span class="required">*</span></label>
            <div class="col-md-2">
							<input type="text" maxlength="20" name="qty" required="required" class="form-control">
						</div>
					</div>

					<div class="ln_solid col-md-12"></div>
					<div class="form-group col-md-8">
						<div class="pull-right">
              <input type="hidden" name="id_produksi" value="<?= $dataProduksi->id_produksi?>">
							<a href="<?= base_url('ProduksiController') ?>"  class="btn btn-default">Cancel</a>
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Daftar Item Produksi</h2>
				<div class="clearfix"></div>
			</div>
            <div class="x_content">
                <table id="datatable" class="table table-striped table-bordered">
					<thead>
						<tr>
							<th>No.</th>
							<th class="col-md-5">ID Barang</th>
							<th class="col-md-3">Nama Barang</th>
							<th class="col-md-3">Qty</th>
							<th class="col-md-3">aksi</th>
						</tr>
					</thead>

					<tbody>
						<?php $nomor = 1; ?>
						<?php if(is_array($dataSubProduksi)): ?>
						<?php foreach ($dataSubProduksi as $key): ?>
							<tr>
								<td><?= $nomor++ ?></td>
								<td><?= $key->kode_barang ?></td>
								<td><?= $key->nama_barang ?></td>
								<td><?= $key->qty ?></td>
								<td align="center">
									<a href="<?= base_url('ProduksiController/deleteSubProduksi/' . $key->id_sub_produksi .'/' . $key->id_produksi ) ?>" onclick="return confirm('Are you sure?')" title="Delete" class="btn btn-danger fa fa-trash"></a>
								</td>
							</tr>
						<?php endforeach ?>
						<?php endif; ?>
					</tbody>
				</table>
            </div>
        </div>
    </div>
</div>

<script>
function goBack() {
    window.history.back();
}
</script>
