<link href="<?= base_url('assets/js/datatables/jquery.dataTables.min.css') ?>" rel="stylesheet" type="text/css" />
<link href="<?= base_url('assets/js/datatables/responsive.bootstrap.min.css') ?>" rel="stylesheet" type="text/css" />

<div class="row">
	<div class="col-md-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Data Produksi</h2>
				<div class="clearfix"></div>
			</div>
			<?php if ($this->session->flashdata('pesan') != null): ?>
                <?php echo $this->session->flashdata('pesan'); ?>
            <?php endif ?>
			<div class="x_content">
				<table id="datatable" class="table table-striped table-bordered">
					<thead>
						<tr>
							<th class="col-md-1">No.</th>
							<th class="col-md-1">Nama Produksi</th>
							<th class="col-md-1">Tgl Produksi</th>
							<th class="col-md-5">Memo</th>
							<th class="col-md-1">Status</th>
						</tr>
					</thead>

					<tbody>
						<?php $nomor = 1; ?>
						<?php foreach ($dataProduksi as $key): ?>
							<tr>
								<td><?= $nomor++ ?></td>
								<td><?= $key->nama_produksi ?></td>
								<td><?= $key->date_produksi ?></td>
								<td><?= $key->memo ?></td>
								<td><?= $key->status ?></td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- Datatables-->
<script src="<?= base_url('assets/js/datatables/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/js/datatables/dataTables.bootstrap.js') ?>"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#datatable').dataTable();
	});
</script>
