<div class="row">
	<div class="col-md-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Tambah Data Produksi</h2>
				<div class="clearfix"></div>
			</div>

			<div class="x_content">
				<form id="demo-form2" data-parsley-validate class="form-horizontal" action="<?= base_url('ProduksiController/insertProduksi') ?>" method="POST">

          <div class="form-group col-md-12">
            <label class="control-label col-md-2">Tgl Produksi <span class="required">*</span></label>
            <div class="col-md-2">
              <?php
              $hari_ini=date("Y-m-d");
              ?>
              <input type="date" name="date_produksi" required="required" min=<?php echo"$hari_ini"; ?> class="form-control">
            </div>
          </div>

          <div class="form-group col-md-12">
						<label class="control-label col-md-2">Nama Produksi <span class="required">*</span></label>
						<div class="col-md-6">
							<input type="text" maxlength="150" name="nama_produksi" required="required" class="form-control">
						</div>
					</div>

          <div class="form-group col-md-12">
						<label class="control-label col-md-2">Memo</label>
						<div class="col-md-6">
              <textarea class="form-control" rows="6" name="memo" placeholder="berikan catatan atau keterangan tentang produksi yang akan dilakukan"></textarea>
						</div>
					</div>

					<div class="ln_solid col-md-12"></div>
					<div class="form-group col-md-8">
						<div class="pull-right">
              <input type="hidden" name="status" value="hold">
							<button type="button" class="btn btn-default" onClick="history.go(-1);return true;">Cancel</button>
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
