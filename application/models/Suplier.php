<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Suplier extends CI_Model {

	private $suplier = "suplier";

	public function getAll() {
		return $this->db->get($this->suplier)->result();
	}

	public function getById($id) {
		$this->db->where('id_suplier', $id);
		$suplier = $this->db->get($this->suplier)->row();

		if ($suplier)
			return $suplier;
		else
			return "null";
	}

	public function insert($nama_suplier, $alamat_suplier, $email_suplier, $tlpn_suplier) {

		// check if suplier duplicate
		$this->db->where('nama_suplier', $nama_suplier);
		$this->db->from($this->suplier);

		$total = $this->db->count_all_results();
		if ($total > 0) { $this->session->set_flashdata('pesan', '<div class="alert alert-danger"><strong>Gagal menambahkan suplier, nama suplier telah terdaftar di database (duplikat email)</strong></div>');
		redirect(base_url().'SuplierController');
		return false;
		}

		$data = array(
			'nama_suplier' => $nama_suplier,
			'alamat_suplier' => $alamat_suplier,
			'email_suplier' => $email_suplier,
			'tlpn_suplier' => $tlpn_suplier,
		);

		$this->db->insert($this->suplier, $data);

		return ($this->db->affected_rows() > 0) ? true : false;
	}

	public function update($id_suplier, $nama_suplier, $alamat_suplier, $email_suplier, $tlpn_suplier) {

		$data = array(
			'nama_suplier' => $nama_suplier,
			'alamat_suplier' => $alamat_suplier,
			'email_suplier' => $email_suplier,
			'tlpn_suplier' => $tlpn_suplier,
		);
		$this->db->where('id_suplier', $id_suplier);
		$this->db->update($this->suplier, $data);

		return ($this->db->affected_rows() > 0) ? true : false;
	}

	public function delete($id_suplier) {

		$totalFirst = $this->db->count_all($this->suplier);

		$this->db->delete($this->suplier, array('id_suplier' => $id_suplier));

		$totalLast = $this->db->count_all($this->suplier);

		return ($totalLast < $totalFirst) ? true : false;
	}

}

/* End of file Suplier.php */
/* Location: ./application/models/Suplier.php */
