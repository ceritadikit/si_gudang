<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Request extends CI_Model {

	private $request = "request";
	private $barang = "barang";
	private $sub_request = "sub_request";

	public function getRequestIndex() {
		$available_status = ['pending', 'terkirim'];

		$this->db->where_in('status', $available_status);
		$this->db->from($this->request);
    return $this->db->get()->result();
	}

	public function getRequestSukses() {
		$this->db->where_in('status', 'disetujui');
		$this->db->from($this->request);
    return $this->db->get()->result();
	}

	public function getRequestDitolak() {
		$this->db->where_in('status', 'ditolak');
		$this->db->from($this->request);
    return $this->db->get()->result();
	}

	public function getAllBarang() {
		$this->db->select('*');
		$this->db->from('barang');
    return $this->db->get()->result();
	}

	public function getById($id) {
		$this->db->select('*');
		$this->db->from('request');
		$this->db->where('id_request', $id);
		$request = $this->db->get()->row();

		if ($request)
			return $request;
		else
			return 'null';
	}

	public function getSubRequest($id) {
		$this->db->select( 'id_sub_request');
		$this->db->select( 'id_request');
		$this->db->select( 'kode_barang');
		$this->db->select( 'nama_barang');
		$this->db->select( 'sub_request.qty');
		$this->db->from('sub_request');
		$this->db->join('barang', 'sub_request.id_barang = barang.id_barang');
		$this->db->where('id_request', $id);
		$request = $this->db->get()->result();

		if ($request != NULL)
			return $request;
		else
			return 'null';
	}



	public function insert($kode_request, $date_request, $deadline, $memo, $status) {

		// check if barang duplicate
		$this->db->where('kode_request', $kode_request);
		$this->db->from($this->request);

		$total = $this->db->count_all_results();
		if ($total > 0) {$this->session->set_flashdata('pesan', '<div class="alert alert-danger"><strong>Gagal membuat request, kode reuest telah digunakan sebelumnya</strong></div>');
		redirect(base_url().'RequestController');
		return false;
		}

		$data = array(
			'kode_request' => $kode_request,
			'date_request' => $date_request,
			'deadline' => $deadline,
			'memo' => $memo,
			'status' => $status,
		);

		$this->db->insert($this->request, $data);

		return ($this->db->affected_rows() > 0) ? true : false;
	}

	public function update($id_request, $kode_request, $deadline, $memo) {
		$data = array(
			'kode_request' => $kode_request,
			'deadline' => $deadline,
			'memo' => $memo,

		);
		$this->db->where('id_request', $id_request);
		$this->db->update($this->request, $data);

		return ($this->db->affected_rows() > 0) ? true : false;
	}

	public function sendRequest($id_request) {
		$data = array(
			'status' => "terkirim",
		);
		$this->db->where('id_request', $id_request);
		$this->db->update($this->request, $data);

		return ($this->db->affected_rows() > 0) ? true : false;
	}

	public function delete($id_request) {

		$totalFirst = $this->db->count_all($this->request);
		$this->db->delete($this->request, array('id_request' => $id_request));
		$totalLast = $this->db->count_all($this->request);

		return ($totalLast < $totalFirst) ? true : false;
	}

	public function insertSubRequest($id_request, $id_barang, $qty) {
		$data = array(
			'id_request' => $id_request,
			'id_barang' => $id_barang,
			'qty' => $qty,
		);
		$this->db->insert($this->sub_request, $data);

		return ($this->db->affected_rows() > 0) ? true : false;
	}

	public function deleteSubRequest($id_sub_request) {
		$totalFirst = $this->db->count_all($this->sub_request);
		$this->db->delete($this->sub_request, array('id_sub_request' => $id_sub_request));
		$totalLast = $this->db->count_all($this->sub_request);

		return ($totalLast < $totalFirst) ? true : false;
	}

}

/* End of file Request.php */
/* Location: ./application/models/Request.php */
