<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Model {

	function cek_login($data) {
		$user = $this->db->get_where('user_auth', $data);

		if($user->num_rows() == 1) {
			$data_session['level'] = $user->row()->level;
			$data_session['id_auth'] = $user->row()->id_auth;
			$this->session->set_userdata($data_session);
			return true;
		} else {
			$this->session->set_flashdata('pesan', '<div class="alert alert-danger"><strong>Login gagal</strong><br>Periksa e-mail atau password Anda!</div>');
			return false;
		}
	}

}

/* End of file Login.php */
/* Location: ./application/models/Login.php */
