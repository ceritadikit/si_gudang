<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produksi extends CI_Model {

	private $barang = "barang";
  private $produksi = "produksi";
	private $sub_produksi = "sub_produksi";

	public function getProduksiIndex() {
		$this->db->where('status', 'hold');
		$this->db->from($this->produksi);
    return $this->db->get()->result();
	}

	public function getRiwayat() {
		$this->db->where_in('status', 'produksi');
		$this->db->from($this->produksi);
    return $this->db->get()->result();
	}

	public function getAllBarang() {
		$this->db->select('*');
		$this->db->from('barang');
    return $this->db->get()->result();
	}

	public function getById($id_produksi) {
		$this->db->select('*');
		$this->db->from('produksi');
		$this->db->where('id_produksi', $id_produksi);
		$produksi = $this->db->get()->row();

		if ($produksi)
			return $produksi;
		else
			return 'null';
	}

	public function getSubProduksi($id_produksi) {
    $this->db->select( 'sub_produksi.id_barang');
		$this->db->select( 'id_sub_produksi');
		$this->db->select( 'id_produksi');
		$this->db->select( 'kode_barang');
		$this->db->select( 'nama_barang');
    $this->db->select( 'barang.qty as qty2');
		$this->db->select( 'sub_produksi.qty');
		$this->db->from('sub_produksi');
		$this->db->join('barang', 'sub_produksi.id_barang = barang.id_barang');
		$this->db->where('id_produksi', $id_produksi);
		$produksi = $this->db->get()->result();

		if ($produksi != NULL)
			return $produksi;
		else
			return 'null';
	}

  public function getPermintaanProduksi() {
		$this->db->where_in('status', 'hold');
		$this->db->from($this->produksi);
    return $this->db->get()->result();
	}



	public function insert($nama_produksi, $date_produksi, $memo, $status) {


		$data = array(
			'nama_produksi' => $nama_produksi,
			'date_produksi' => $date_produksi,
			'memo' => $memo,
			'status' => $status,
		);

		$this->db->insert($this->produksi, $data);

		return ($this->db->affected_rows() > 0) ? true : false;
	}

	public function update($id_produksi, $nama_produksi, $date_produksi, $memo) {
		$data = array(
			'nama_produksi' => $nama_produksi,
			'date_produksi' => $date_produksi,
			'memo' => $memo,

		);
		$this->db->where('id_produksi', $id_produksi);
		$this->db->update($this->produksi, $data);
		return ($this->db->affected_rows() > 0) ? true : false;
	}

	public function sendProduksi($id_request) {
		$data = array(
			'status' => "terkirim",
		);
		$this->db->where('id_request', $id_request);
		$this->db->update($this->request, $data);

		return ($this->db->affected_rows() > 0) ? true : false;
	}

	public function delete($id_produksi) {

		$totalFirst = $this->db->count_all($this->produksi);
		$this->db->delete($this->produksi, array('id_produksi' => $id_produksi));
		$totalLast = $this->db->count_all($this->produksi);

		return ($totalLast < $totalFirst) ? true : false;
	}

	public function insertSubProduksi($id_produksi, $explode_id, $qty, $explode_qty) {

    if ($explode_qty < $qty) {
      $this->session->set_flashdata('pesan', '<div class="alert alert-danger"><strong>Gagal menambahkan item produksi, kebutuhan barang melebihi stock yang tersedia di gudang, saat ini hanya tersedia '.$qty.'segera buat surat pemintaan barang ke bagian gudang. </strong></div>');
		  redirect(base_url().'ProduksiController/addSubProduksi/'.$id_produksi);
		return false;
    } else {
      $data = array(
  			'id_produksi' => $id_produksi,
  			'id_barang' => $explode_id,
  			'qty' => $qty,
  		);
  		$this->db->insert($this->sub_produksi, $data);
  		return ($this->db->affected_rows() > 0) ? true : false;
    }
	}

	public function deleteSubProduksi($id_sub_produksi) {
		$totalFirst = $this->db->count_all($this->sub_produksi);
		$this->db->delete($this->sub_produksi, array('id_sub_produksi' => $id_sub_produksi));
		$totalLast = $this->db->count_all($this->sub_produksi);

		return ($totalLast < $totalFirst) ? true : false;
	}

  public function ubahStatus($id_produksi) {
		$data = array(
			'status' => "produksi",
		);

		$this->db->where('id_produksi', $id_produksi);
		$this->db->update($this->produksi, $data);

		return ($this->db->affected_rows() > 0) ? true : false;
	}

  public function approvalProduksi($id_produksi, $id_barang, $qty_jum) {
    $data = array(
			'qty' => $qty_jum,
			);

    $this->db->where('id_barang', $id_barang);
		$this->db->update($this->barang, $data);

		return ($this->db->affected_rows() > 0) ? true : false;
	}

}

/* End of file Produksi.php */
/* Location: ./application/models/Produksi.php */
