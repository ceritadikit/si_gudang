<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Model {

	private $barang = "barang";
	private $suplier = "suplier";

	public function getAll() {

		$this->db->select('*');
		$this->db->from('barang');
		$this->db->join('suplier', 'barang.id_suplier = suplier.id_suplier');

		return $this->db->get()->result();
	}

	public function getAllSuplier() {
		return $this->db->get('suplier');
	}

	public function getById($id) {
		$this->db->select('*');
		$this->db->from('barang');
		$this->db->join('suplier', 'barang.id_suplier = suplier.id_suplier');
		$this->db->where('id_barang', $id);
		$barang = $this->db->get()->row();

		if ($barang)
			return $barang;
		else
			return 'null';
	}

	public function insert($kode_barang, $nama_barang, $qty, $rak, $id_suplier) {

		// check if barang duplicate
		$this->db->where('kode_barang', $kode_barang);
		$this->db->from($this->barang);

		$total = $this->db->count_all_results();
		if ($total > 0) {$this->session->set_flashdata('pesan', '<div class="alert alert-danger"><strong>Gagal menambahkan barang, kode barang telah terdaftar di database</strong></div>');
		redirect(base_url().'BarangController');
		return false;
		}

		$data = array(
			'kode_barang' => $kode_barang,
			'nama_barang' => $nama_barang,
			'qty' => $qty,
			'rak' => $rak,
			'id_suplier' => $id_suplier,
		);

		$this->db->insert($this->barang, $data);

		return ($this->db->affected_rows() > 0) ? true : false;
	}

	public function update($id_barang, $kode_barang, $nama_barang, $qty, $rak, $id_suplier) {
		$data = array(
			'kode_barang' => $kode_barang,
			'nama_barang' => $nama_barang,
			'qty' => $qty,
			'rak' => $rak,
			'id_suplier' => $id_suplier,
		);
		$this->db->where('id_barang', $id_barang);
		$this->db->update($this->barang, $data);

		return ($this->db->affected_rows() > 0) ? true : false;
	}

	public function delete($id_barang) {

		$totalFirst = $this->db->count_all($this->barang);

		$this->db->delete($this->barang, array('id_barang' => $id_barang));

		$totalLast = $this->db->count_all($this->barang);

		return ($totalLast < $totalFirst) ? true : false;
	}

}

/* End of file Barang.php */
/* Location: ./application/models/Barang.php */
