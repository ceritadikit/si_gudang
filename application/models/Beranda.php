<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Model {

    /**
     * status email : Pending dan Terkirim
     */
    public function getEmailByStatus($status = 'Pending') {
        $this->db->where('status', $status);
        $this->db->order_by('tgl_kirim', 'ASC');
        return $this->db->get('item')->result();
    }

}

/* End of file Beranda.php */
/* Location: ./application/models/Beranda.php */
