<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RequestController extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Request');
	}

	private function auth() {
		if($this->session->userdata('level') == null) {
			redirect(base_url());
		}
	}

	public function index() {
		$this->auth();

		$data['content'] = 'gudang/v_request';
		$data['dataRequest'] = $this->Request->getRequestIndex();
		$this->load->view('template/gudang/v_template', $data);
	}

	public function sukses() {
		$this->auth();

		$data['content'] = 'gudang/v_request_sukses';
		$data['dataRequest'] = $this->Request->getRequestSukses();
		$this->load->view('template/gudang/v_template', $data);
	}

	public function ditolak() {
		$this->auth();

		$data['content'] = 'gudang/v_request_ditolak';
		$data['dataRequest'] = $this->Request->getRequestDitolak();
		$this->load->view('template/gudang/v_template', $data);
	}

	public function addRequest() {
		$this->auth();

		$data['content'] = 'gudang/v_add_request';
		$this->load->view('template/gudang/v_template', $data);
	}

	public function editRequest($id) {
		$this->auth();

		$data['content'] = 'gudang/v_edit_request';
		$data['dataRequest'] = $this->Request->getById($id);
		$this->load->view('template/gudang/v_template', $data);
	}


	public function insertRequest() {
		$this->auth();

		// get parameter
			$kode_request = $this->input->post('kode_request');
			$date_request = $this->input->post('date_request');
			$deadline = $this->input->post('deadline');
			$memo = $this->input->post('memo');
			$status = $this->input->post('status');
		// insert
		$this->Request->insert($kode_request, $date_request, $deadline, $memo, $status);

		redirect(base_url().'RequestController');
	}

	public function updateRequest() {
		$this->auth();

		// get parameter
		$id_request = $this->input->input_stream('id_request');
		$kode_request = $this->input->input_stream('kode_request');
		$deadline = $this->input->input_stream('deadline');
		$memo = $this->input->input_stream('memo');

		// update
		$this->Request->update($id_request, $kode_request, $deadline, $memo);

		redirect(base_url().'RequestController');
	}

	public function sendRequest($id_request) {
		$this->auth();

		$this->Request->sendRequest($id_request);

		redirect(base_url().'RequestController');
	}

	public function deleterequest($id_request) {
		$this->auth();

		// get all user
		$this->Request->delete($id_request);
		redirect(base_url().'RequestController');
	}

	public function addSubRequest($id) {
		$this->auth();

		$data['content'] = 'gudang/v_add_sub_request';
		$data['dataBarang'] = $this->Request->getAllBarang();
		$data['dataRequest'] = $this->Request->getById($id);
		$data['dataSubRequest'] = $this->Request->getSubRequest($id);
		$this->load->view('template/gudang/v_template', $data);
	}

	public function insertSubRequest() {
		$this->auth();

		// get parameter
			$id_request = $this->input->post('id_request');
			$id_barang = $this->input->post('id_barang');
			$qty = $this->input->post('qty');
		// insert

		$this->Request->insertSubRequest($id_request, $id_barang, $qty);

		redirect(base_url().'RequestController/addSubRequest/'.$id_request);
	}

	public function deleteSubRequest($id_sub_request, $id_request) {
		$this->auth();

		// get all user
		$this->Request->deleteSubRequest($id_sub_request);

		redirect(base_url().'RequestController/addSubRequest/'.$id_request);
	}

}

/* End of file RequestController.php */
/* Location: ./application/controllers/RequestController.php */
