<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProduksiController extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Produksi');
	}

	private function auth() {
		if($this->session->userdata('level') == null) {
			redirect(base_url());
		}
	}

	public function index() {
		$this->auth();

		$data['content'] = 'produksi/v_produksi';
		$data['dataProduksi'] = $this->Produksi->getProduksiIndex();
		$this->load->view('template/produksi/v_template', $data);
	}

	public function sukses() {
		$this->auth();

		$data['content'] = 'gudang/v_request_sukses';
		$data['dataRequest'] = $this->Request->getRequestSukses();
		$this->load->view('template/gudang/v_template', $data);
	}

	public function ditolak() {
		$this->auth();

		$data['content'] = 'gudang/v_request_ditolak';
		$data['dataRequest'] = $this->Request->getRequestDitolak();
		$this->load->view('template/gudang/v_template', $data);
	}

	public function addProduksi() {
		$this->auth();

		$data['content'] = 'produksi/v_add_produksi';
		$this->load->view('template/produksi/v_template', $data);
	}

	public function editProduksi($id_produksi) {
		$this->auth();

		$data['content'] = 'produksi/v_edit_produksi';
		$data['dataProduksi'] = $this->Produksi->getById($id_produksi);
		$this->load->view('template/produksi/v_template', $data);
	}


	public function insertProduksi() {
		$this->auth();

		// get parameter
			$nama_produksi = $this->input->post('nama_produksi');
			$date_produksi = $this->input->post('date_produksi');
			$memo = $this->input->post('memo');
			$status = $this->input->post('status');
		// insert
		$this->Produksi->insert($nama_produksi, $date_produksi, $memo, $status);

		redirect(base_url().'ProduksiController');
	}

	public function updateProduksi() {
		$this->auth();

		// get parameter
		$id_produksi = $this->input->input_stream('id_produksi');
		$nama_produksi = $this->input->input_stream('nama_produksi');
		$date_produksi = $this->input->input_stream('date_produksi');
		$memo = $this->input->input_stream('memo');

		// update
		$this->Produksi->update($id_produksi, $nama_produksi, $date_pr, $memo);

		redirect(base_url().'ProduksiController');
	}

  public function ditailItemProduksi($id_produksi) {
    $this->auth();
    $data['content'] = 'produksi/v_approval';
    $data['dataProduksi'] = $this->Produksi->getById($id_produksi);
    $data['dataPermintaan'] = $this->Produksi->getPermintaanProduksi();
    $data['dataSubProduksi'] = $this->Produksi->getSubProduksi($id_produksi);
    $this->load->view('template/produksi/v_template', $data);
  }

  public function approvalProduksi() {
    $this->auth();

    // get parameter
    $id_produksi = $this->input->post('id_produksi');
    $id_barang = $this->input->post('id_barang');
    $qty = $this->input->post('qty');
    $qty2 = $this->input->post('qty2');

    $count = count ($this->input->post('qty'));
    if ($id_produksi != NULL){
    $this->Produksi->ubahStatus($id_produksi);
      for ($i=1; $i <= $count; $i++) {
        $qty_jum[$i] =  $qty2[$i] - $qty[$i];
        $this->Produksi->approvalProduksi($id_produksi, $id_barang[$i], $qty_jum[$i]);
      }
    }
    redirect(base_url().'ProduksiController');
  }

	public function deleteProduksi($id_produksi) {
		$this->auth();

		// get all user
		$this->Produksi->delete($id_produksi);
		redirect(base_url().'ProduksiController');
	}

	public function addSubProduksi($id) {
		$this->auth();

		$data['content'] = 'produksi/v_add_sub_produksi';
		$data['dataBarang'] = $this->Produksi->getAllBarang();
		$data['dataProduksi'] = $this->Produksi->getById($id);
		$data['dataSubProduksi'] = $this->Produksi->getSubProduksi($id);
		$this->load->view('template/produksi/v_template', $data);
	}

	public function insertSubProduksi() {
		$this->auth();

		// get parameter
			$id_produksi = $this->input->post('id_produksi');
			$id_barang = $this->input->post('id_barang');
			$qty = $this->input->post('qty');

      $explode_id_qty = explode('|', $id_barang);
      $explode_id =  $explode_id_qty[0];
      $explode_qty =  $explode_id_qty[1];
		// insert


		$this->Produksi->insertSubProduksi($id_produksi, $explode_id, $qty, $explode_qty);

		redirect(base_url().'ProduksiController/addSubProduksi/'.$id_produksi);
	}

	public function deleteSubProduksi($id_sub_produksi, $id_produksi) {
		$this->auth();

		// get all user
		$this->Produksi->deleteSubProduksi($id_sub_produksi);

		redirect(base_url().'ProduksiController/addSubProduksi/'.$id_produksi);
	}

  public function riwayat() {
		$this->auth();

		$data['content'] = 'produksi/v_riwayat_produksi';
		$data['dataProduksi'] = $this->Produksi->getRiwayat();
		$this->load->view('template/produksi/v_template', $data);
	}

  public function stock() {
		$this->auth();

		$data['content'] = 'produksi/v_stock';
		$data['dataBarang'] = $this->Produksi->getAllBarang();
		$this->load->view('template/produksi/v_template', $data);
	}

}

/* End of file ProduksiController.php */
/* Location: ./application/controllers/ProduksiController.php */
