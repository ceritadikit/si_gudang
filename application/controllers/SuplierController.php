<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SuplierController extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Suplier');
	}

	private function auth() {
		if($this->session->userdata('level') == null) {
			redirect(base_url());
		}
	}

	public function index() {
		$this->auth();

		$data['content'] = 'suplier/v_index';
		$data['dataSuplier'] = $this->Suplier->getAll();
		$this->load->view('template/gudang/v_template', $data);
	}

	public function addSuplier() {
		$this->auth();

		$data['content'] = 'suplier/v_tambah';
		$this->load->view('template/gudang/v_template', $data);
	}

	public function editSuplier($id) {
		$this->auth();

		$data['content'] = 'suplier/v_edit';
		$data['dataSuplier'] = $this->Suplier->getById($id);
		$this->load->view('template/gudang/v_template', $data);
	}

	public function insertSuplier() {
		$this->auth();

		// get parameter
		$nama_suplier= $this->input->post('nama_suplier');
		$alamat_suplier = $this->input->post('alamat_suplier');
		$email_suplier = $this->input->post('email_suplier');
		$tlpn_suplier = $this->input->post('tlpn_suplier');

		// insert
		$this->Suplier->insert($nama_suplier, $alamat_suplier, $email_suplier, $tlpn_suplier);

		redirect(base_url().'suplier');
	}

	public function updateSuplier() {
		$this->auth();

		// get parameter
		$id_suplier = $this->input->input_stream('id_suplier');
		$nama_suplier= $this->input->input_stream('nama_suplier');
		$alamat_suplier = $this->input->input_stream('alamat_suplier');
		$email_suplier = $this->input->input_stream('email_suplier');
		$tlpn_suplier = $this->input->input_stream('tlpn_suplier');

		// update
		$this->Suplier->update($id_suplier, $nama_suplier, $alamat_suplier, $email_suplier, $tlpn_suplier);

		redirect(base_url().'suplier');
	}

	public function deleteSuplier($id_suplier) {
		$this->auth();

		// get all user
		$this->Suplier->delete($id_suplier);

		redirect(base_url().'suplier');
	}

}

/* End of file SuplierController.php */
/* Location: ./application/controllers/SuplierController.php */
