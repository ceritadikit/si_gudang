<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BarangController extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Barang');
	}

	private function auth() {
		if($this->session->userdata('level') == null) {
			redirect(base_url());
		}
	}

	public function index() {
		$this->auth();

		$data['content'] = 'gudang/v_index';
		$data['dataBarang'] = $this->Barang->getAll();
		$this->load->view('template/gudang/v_template', $data);
	}

	public function addBarang() {
		$this->auth();

		$data['content'] = 'gudang/v_add';
		$data['dataSuplier'] = $this->Barang->getAllSuplier();
		$this->load->view('template/gudang/v_template', $data);
	}

	public function editBarang($id) {
		$this->auth();

		$data['content'] = 'gudang/v_edit';
		$data['dataBarang'] = $this->Barang->getById($id);
		$data['dataSuplier'] = $this->Barang->getAllSuplier();
		$this->load->view('template/gudang/v_template', $data);
	}

	public function insertBarang() {
		$this->auth();

		// get parameter
			$kode_barang = $this->input->post('kode_barang');
			$nama_barang = $this->input->post('nama_barang');
			$qty = $this->input->post('qty');
			$rak = $this->input->post('rak');
			$id_suplier = $this->input->post('id_suplier');
		// insert
		$this->Barang->insert($kode_barang, $nama_barang, $qty, $rak, $id_suplier);

		redirect(base_url().'BarangController');
	}

	public function updateBarang() {
		$this->auth();

		// get parameter
		$id_barang = $this->input->input_stream('id_barang');
		$kode_barang = $this->input->input_stream('kode_barang');
		$nama_barang = $this->input->input_stream('nama_barang');
		$qty = $this->input->input_stream('qty');
		$rak = $this->input->input_stream('rak');
		$id_suplier = $this->input->input_stream('id_suplier');

		// update
		$this->Barang->update($id_barang, $kode_barang, $nama_barang, $qty, $rak, $id_suplier);

		redirect(base_url().'BarangController');
	}

	public function deleteBarang($id_barang) {
		$this->auth();

		// get all user
		$this->Barang->delete($id_barang);

		redirect(base_url().'BarangController');
	}

}

/* End of file BarangController.php */
/* Location: ./application/controllers/BarangController.php */
