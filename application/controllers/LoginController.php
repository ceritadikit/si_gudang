<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginController extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Login');
	}

	public function index()
	{
		if($this->session->userdata('level') != null) {
			redirect(base_url('BerandaController'));
		}

		$data['content'] = 'login/v_Login';
		$this->load->view('login/v_Login', $data);
	}

	public function login()
	{
		$data = array(
			'email' => $this->input->post('email'),
			'password' => ($this->input->post('password'))
		);

		$hasil = $this->Login->cek_login($data);
		if($hasil)
			redirect(base_url().'BerandaController');
		else
			redirect(base_url().'LoginController');
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url());
	}

}

/* End of file LoginController.php */
/* Location: ./application/controllers/BerandaController.php */
