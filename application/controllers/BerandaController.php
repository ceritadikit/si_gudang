<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BerandaController extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Beranda');
	}

	private function auth() {
		if($this->session->userdata('level') == null) {
			redirect(base_url());
		}
	}

	public function index()
	{
		if ($this->session->userdata('level') == "keuangan") {
			$this->auth();
			$data['content'] = 'beranda/v_beranda_keuangan';
			$this->load->view('template/keuangan/v_template', $data);
		} elseif ($this->session->userdata('level') == "gudang") {
			$this->auth();
			$data['content'] = 'beranda/v_beranda_gudang';
			$this->load->view('template/gudang/v_template', $data);
		} elseif ($this->session->userdata('level') == "produksi") {
			$this->auth();
			$data['content'] = 'beranda/v_beranda_produksi';
			$this->load->view('template/produksi/v_template', $data);
		}
		 else {
			redirect(base_url());
		}
	}

}

/* End of file BerandaController.php */
/* Location: ./application/controllers/BerandaController.php */
